# SwipeCaptcha_ohos

**本项目是基于开源项目SwipeCaptcha进行鸿蒙化的移植和开发的，可以通过项目标签以及github地址 （ https://github.com/mcxtzhang/SwipeCaptcha ）追踪到原安卓项目版本，该项目的讲解介绍已在社区发布，可以通过网址（ https://harmonyos.51cto.com/posts/3402 ）访问相关内容。**

#### 项目介绍

- 项目名称：滑动验证码
- 所属系列：鸿蒙的第三方组件适配移植
- 功能：实现了滑动验证的功能     可以判断验证成功或者是失败
- 项目移植状态：80%，移植完成组件的核心功能——图片验证，未移植：滑块透明度部分内容。
- 调用差异：重构
- 开发版本：sdk5，DevEco Studio2.1 beta3
- 项目作者和维护人：赵柏屹
- 邮箱：[isrc_hm@iscas.ac.cn](mailto:isrc_hm@iscas.ac.cn)
- 原项目Doc地址：https://github.com/mcxtzhang/SwipeCaptcha
- 目前该项目已经更新至2.0版本，已经完成全部功能的移植，由于部分原因暂不能将项目合并（https://gitee.com/isrc_ohos/swipe-captcha_ohos2.0）

![输入图片说明](https://images.gitee.com/uploads/images/2021/0312/152758_ca28cdc9_8496150.gif "123.gif")

#### 项目介绍

- 编程语言：Java
- 本项目为一个Abilityslice界面 没有单独封装调用 使用时需要把代码复制到所需要使用的位置即可；原安卓组件也并没有封装成独立的组件也是写在了Ability里面
- 该组件可以实现滑动拼图的验证方式，操作简单，安全性强，被众多APP使用。目前虽然不支持滑块透明度显示，但是核心功能已经移植完成。

#### 使用教程

1. 初始化数据，包含获取手机屏幕宽度信息、设置进度值也验证状态的初试提示文字、初始化画笔信息、定义画笔属性。

```java
//屏幕宽度
//获取手机屏幕大小为displayAttributes.width
DisplayManager displayManager = DisplayManager.getInstance();
Display display = displayManager.getDefaultDisplay(this).get();
DisplayAttributes displayAttributes = display.getAttributes();
//定义文字
text = new Text(this);
text2 = new Text(this);
//定义画笔
mPaint = new Paint();
```

2. 画左侧抠块拼图函数

```java
 public void onDraw(Component component, Canvas canvas) {

        canvas.translate(slider.getProgress()*displayAttributes.width /100 - 100 , top);
        canvas.scale(ratio , ratio);
        canvas.drawPixelMapHolder(pixelMapHolder1 , 0 , 0 , mPaint);

}；
```

3. 画最终位置边框

```java
public void onDraw(Component component, Canvas canvas) {
        //依次画四条线 左上左下
        canvas.drawLine(new Point(puzzel2left , top),
                new Point(puzzel2left, top+200), mPaint);
        //左上右上
        canvas.drawLine(new Point(puzzel2left, top),
                new Point(puzzel2left+200, top), mPaint);
        //右上右下
        canvas.drawLine(new Point(puzzel2left+200, top),
                new Point(puzzel2left+200, top+200), mPaint);
        //左下右下
        canvas.drawLine(new Point(puzzel2left, top+200),
                new Point(puzzel2left+200, top+200), mPaint);
    }；
```

4. 同时还要初始化拖动条，每次重新拖动则更新拖动条的初始位置，然后将抠块和进度条通过监听事件绑定。

   ```java
   slider.setValueChangedListener(new Slider.ValueChangedListener() {
       .....
   })
   ```

   

#### 版本迭代

- v0.1.0-alpha

#### 版权和许可信息

- SwipeCaptcha_ohos经过[Apache License, version 2.0](http://www.apache.org/licenses/LICENSE-2.0)授权许可。

 

 